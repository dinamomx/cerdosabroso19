/**
 * Herramienta de logeo local para depuración sin usar el nativo console
 */
// eslint-disable-next-line
import Debug from 'debug'

const debug = Debug('app:log')
const error = Debug('app:error')
const warn = Debug('app:warn')

debug.color = process.server ? 6 : Debug.colors[9]
error.color = process.server ? 5 : Debug.colors[60]
warn.color = process.server ? 6 : Debug.colors[75]

export default ({ app }, inject) => {
  app.$log = {
    log: debug,
    debug,
    error,
    warn,
  }

  inject('log', {
    log: debug,
    debug,
    error,
    warn,
  })
}
