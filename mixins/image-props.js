export default {
  /**
   * La url o path de la imagen
   *
   * @example '/placeholder/demo.jpg'
   */
  src: {
    type: String,
    required: true,
  },
  /**
   * El texto alternativo de la imagen
   * @example 'La cara de una niña'
   */
  alt: {
    type: String,
    default: 'Placeholder, please fill me',
  },
}
