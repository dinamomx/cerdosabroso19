const isServer = !process.client

module.exports = {
  presets: [
    [
      '@nuxt/babel-preset-app',
      {
        loose: true,
        targets: isServer ? { node: 'current' } : {},
      },
    ],
  ],
  plugins: [
    // Reduce drásticamente el tamaño del bundle
    'lodash',
  ],
}
