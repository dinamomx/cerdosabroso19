import path from 'path'

// TODO: Remove when new release of Vue (https://github.com/nuxt/nuxt.js/issues/4312)
const staticClassHotfix = el => {
  el.staticClass = el.staticClass && el.staticClass.replace(/\\[a-z]\b/g, '')
  if (Array.isArray(el.children)) {
    el.children.map(staticClassHotfix)
  }
}

const postCssConfig = {
  plugins: {
    'postcss-preset-env': {},
  },
}

const webpackConfig = {
  resolve: {
    extensions: ['.wasm', '.mjs', '.js', '.json', '.vue', '.jsx'],
    alias: {
      '~': path.resolve(__dirname),
      '~~': path.resolve(__dirname),
      '@': path.resolve(__dirname),
      '@@': path.resolve(__dirname),
      assets: path.resolve(__dirname, 'assets'),
      static: path.resolve(__dirname, 'static'),
    },
    modules: ['node_modules', path.resolve(__dirname, 'node_modules')],
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          transformAssetUrls: {
            video: 'src',
            source: 'src',
            object: 'src',
            embed: 'src',
          },
          productionMode: false,
          compilerOptions: {
            modules: [
              {
                postTransformNode: staticClassHotfix,
              },
            ],
          },
        },
      },
      {
        test: /\.pug$/,
        oneOf: [
          {
            resourceQuery: /^\?vue/,
            use: [
              {
                loader: 'pug-plain-loader',
                options: {},
              },
            ],
          },
          {
            use: [
              'raw-loader',
              {
                loader: 'pug-plain-loader',
                options: {},
              },
            ],
          },
        ],
      },
      {
        test: /\.jsx?$/,
        exclude: file => {
          // not exclude files outside node_modules
          if (!/node_modules/.test(file)) {
            return false
          }

          // item in transpile can be string or regex object
          return !this.modulesToTranspile.some(module => module.test(file))
        },
        use: [
          {
            loader: 'babel-loader',
            options: {
              babelrc: false,
              cacheDirectory: true,
              presets: [
                [
                  '@nuxt/babel-preset-app',
                  {
                    buildTarget: 'server',
                  },
                ],
              ],
            },
          },
        ],
      },
      {
        test: /\.css$/,
        oneOf: [
          {
            resourceQuery: /module/,
            use: [
              {
                loader: 'vue-style-loader',
                options: undefined,
              },
              {
                loader: 'css-loader',
                options: {
                  localIdentName: '[local]_[hash:base64:5]',
                  sourceMap: true,
                  importLoaders: 2,
                  modules: true,
                },
              },
              {
                loader: 'postcss-loader',
                options: postCssConfig,
              },
            ],
          },
          {
            use: [
              {
                loader: 'vue-style-loader',
                options: undefined,
              },
              {
                loader: 'css-loader',
                options: {
                  sourceMap: true,
                  importLoaders: 2,
                },
              },
              {
                loader: 'postcss-loader',
                options: postCssConfig,
              },
            ],
          },
        ],
      },
      {
        test: /\.less$/,
        oneOf: [
          {
            resourceQuery: /module/,
            use: [
              {
                loader: 'vue-style-loader',
                options: undefined,
              },
              {
                loader: 'css-loader',
                options: {
                  localIdentName: '[local]_[hash:base64:5]',
                  sourceMap: true,
                  importLoaders: 2,
                  modules: true,
                },
              },
              {
                loader: 'postcss-loader',
                options: postCssConfig,
              },
              {
                loader: 'less-loader',
                options: {
                  sourceMap: true,
                },
              },
            ],
          },
          {
            use: [
              {
                loader: 'vue-style-loader',
                options: undefined,
              },
              {
                loader: 'css-loader',
                options: {
                  sourceMap: true,
                  importLoaders: 2,
                },
              },
              {
                loader: 'postcss-loader',
                options: {
                  sourceMap: true,
                  options: postCssConfig,
                },
              },
              {
                loader: 'less-loader',
                options: {
                  sourceMap: true,
                },
              },
            ],
          },
        ],
      },
      {
        test: /\.sass$/,
        oneOf: [
          {
            resourceQuery: /module/,
            use: [
              {
                loader: 'vue-style-loader',
                options: undefined,
              },
              {
                loader: 'css-loader',
                options: {
                  localIdentName: '[local]_[hash:base64:5]',
                  sourceMap: true,
                  importLoaders: 2,
                  modules: true,
                },
              },
              {
                loader: 'postcss-loader',
                options: postCssConfig,
              },
              {
                loader: 'sass-loader',
                options: {
                  indentedSyntax: true,
                  sourceMap: true,
                },
              },
            ],
          },
          {
            use: [
              {
                loader: 'vue-style-loader',
                options: undefined,
              },
              {
                loader: 'css-loader',
                options: {
                  sourceMap: true,
                  importLoaders: 2,
                },
              },
              {
                loader: 'postcss-loader',
                options: postCssConfig,
              },
              {
                loader: 'sass-loader',
                options: {
                  indentedSyntax: true,
                  sourceMap: true,
                },
              },
            ],
          },
        ],
      },
      {
        test: /\.scss$/,
        oneOf: [
          {
            resourceQuery: /module/,
            use: [
              {
                loader: 'vue-style-loader',
                options: undefined,
              },
              {
                loader: 'css-loader',
                options: {
                  localIdentName: '[local]_[hash:base64:5]',
                  sourceMap: true,
                  importLoaders: 2,
                  modules: true,
                },
              },
              {
                loader: 'postcss-loader',
                options: postCssConfig,
              },
              {
                loader: 'sass-loader',
                options: {
                  sourceMap: true,
                },
              },
            ],
          },
          {
            use: [
              {
                loader: 'vue-style-loader',
                options: undefined,
              },
              {
                loader: 'css-loader',
                options: {
                  sourceMap: true,
                  importLoaders: 2,
                },
              },
              {
                loader: 'postcss-loader',
                options: postCssConfig,
              },
              {
                loader: 'sass-loader',
                options: {
                  sourceMap: true,
                },
              },
            ],
          },
        ],
      },
      {
        test: /\.styl(us)?$/,
        oneOf: [
          {
            resourceQuery: /module/,
            use: [
              {
                loader: 'vue-style-loader',
                options: undefined,
              },
              {
                loader: 'css-loader',
                options: {
                  localIdentName: '[local]_[hash:base64:5]',
                  sourceMap: true,
                  importLoaders: 2,
                  modules: true,
                },
              },
              {
                loader: 'postcss-loader',
                options: postCssConfig,
              },
              {
                loader: 'stylus-loader',
                options: {
                  sourceMap: true,
                },
              },
            ],
          },
          {
            use: [
              {
                loader: 'vue-style-loader',
                options: undefined,
              },
              {
                loader: 'css-loader',
                options: {
                  sourceMap: true,
                  importLoaders: 2,
                },
              },
              {
                loader: 'postcss-loader',
                options: postCssConfig,
              },
              {
                loader: 'stylus-loader',
                options: {
                  sourceMap: true,
                },
              },
            ],
          },
        ],
      },
      {
        test: /\.(png|jpe?g|gif|svg|webp)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 1000,
              name: '[path][name].[ext]',
            },
          },
        ],
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 1000,
              name: '[path][name].[ext]',
            },
          },
        ],
      },
      {
        test: /\.(webm|mp4|ogv)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]',
            },
          },
        ],
      },
    ],
  },
  // resolveLoader: clientConfig.resolveLoader,
}

module.exports = {
  require: [
    path.join(__dirname, 'styleguide.global.requires.js'),
    path.join(__dirname, 'assets/sass/app.scss'),
  ],
  components: 'components/**/*.vue',
  webpackConfig,
  getComponentPathLine(componentPath) {
    const name = path.basename(componentPath, '.vue')
    const dir = path.dirname(componentPath)
    return `import ${name} from '~/${dir}/${name}';`
  },
  showUsage: true,

  vuex: path.resolve(__dirname, 'store/index'),
}
