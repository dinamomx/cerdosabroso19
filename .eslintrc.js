module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    es6: true
  },
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 2018,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  extends: [
    'airbnb-base',
    'plugin:prettier/recommended',
    'plugin:vue/recommended',
    'prettier/vue',
  ],
  plugins: [
    'prettier',
    'vue',
    'jsdoc',
  ],
  settings: {
    'import/resolver': {
      webpack: 'webpack.config.js',
    }
  },
  rules: {
    // Reglas genéricas que queremos añadir
      quotes: [
        'error',
        'single',
        {
          avoidEscape: true,
          allowTemplateLiterals: false
        }
      ],
      semi: ['error', 'never'],
      'no-console': 1,
      'no-debugger': 1,
      'import/extensions': ['error', 'always', {
        js: 'never',
        vue: 'always'
      }],
      'max-len': ['error', {
        code: 80,
        ignoreUrls: true
      }],
      'no-plusplus': ['error', {
        allowForLoopAfterthoughts: true
      }],
      'no-param-reassign': ['error', {
        props: false
      }],
      // Reglas de JSDOC
      "jsdoc/check-examples": 1,
      "jsdoc/check-param-names": 1,
      "jsdoc/check-tag-names": 1,
      "jsdoc/check-types": 1,
      "jsdoc/newline-after-description": 1,
      "jsdoc/no-undefined-types": 1,
      "jsdoc/require-description": 0,
      "jsdoc/require-description-complete-sentence": 1,
      "jsdoc/require-example": 0,
      "jsdoc/require-hyphen-before-param-description": 1,
      "jsdoc/require-param": 1,
      "jsdoc/require-param-description": 1,
      "jsdoc/require-param-name": 1,
      "jsdoc/require-param-type": 1,
      "jsdoc/require-returns": 1,
      "jsdoc/require-returns-check": ['error'],
      "jsdoc/require-returns-description": 1,
      "jsdoc/require-returns-type": 1,
      "jsdoc/valid-types": 1,
      // Reglas de vue
      'vue/html-closing-bracket-newline': ['error', {
        singleline: 'never',
        multiline: 'never'
      }],
      'vue/mustache-interpolation-spacing': ['error', 'always'],
      'vue/html-indent': ['error', 2, {
        attribute: 1,
        closeBracket: 0,
        alignAttributesVertically: true,
        ignores: ['svg']
      }],
      'vue/max-attributes-per-line': [2, {
        singleline: 1,
        multiline: {
          max: 1,
          allowFirstLine: true
        }
      }],
      'vue/html-closing-bracket-spacing': ['error', {
        startTag: 'never',
        endTag: 'never',
        selfClosingTag: 'always'
      }],
      'vue/html-quotes': ['error', 'double'],
      'vue/component-name-in-template-casing': [
        'error',
        'kebab-case',
      ],
      'vue/order-in-components': ['error', {
        order: [
          'el',
          'name',
          'parent',
          'functional',
          'middleware',
          ['delimiters', 'comments'],
          'head',
          ['components', 'directives', 'filters'],
          'extends',
          'mixins',
          'validate',
          'inheritAttrs',
          'model',
          ['props', 'propsData'],
          ['asyncData', 'fetch'],
          'data',
          'computed',
          'watch',
          'watchQuery',
          'LIFECYCLE_HOOKS',
          'methods',
          'layout',
          ['template', 'render'],
          'scrollToTop',
          'transition',
          'renderError',
        ]
      }],
      'vue/html-self-closing': [
        'error',
        {
          html: {
            void: 'any',
            "normal": "always",
            "component": "always"
          },
          "svg": "always",
          "math": "always"
        }
      ],
      'vue/no-multi-spaces': ['error', {
        ignoreProperties: true
      }],
      'vue/no-spaces-around-equal-signs-in-attribute': ['error'],
      'vue/require-default-prop': [2],
      'vue/require-prop-types': [2],
  }
}
